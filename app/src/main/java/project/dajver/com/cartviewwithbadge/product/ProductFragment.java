package project.dajver.com.cartviewwithbadge.product

import android.content.Intent
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import butterknife.BindView
import project.dajver.com.cartviewwithbadge.BaseFragment
import project.dajver.com.cartviewwithbadge.R
import project.dajver.com.cartviewwithbadge.cart.CartActivity
import project.dajver.com.cartviewwithbadge.cart.helper.CartHelper
import project.dajver.com.cartviewwithbadge.cart.helper.entity.models.ProductEntityModel
import project.dajver.com.cartviewwithbadge.etc.ProductsHelper.productsList
import project.dajver.com.cartviewwithbadge.product.adapter.ProductRecyclerAdapter
import project.dajver.com.cartviewwithbadge.product.adapter.models.ProductModel
import java.math.BigDecimal

class ProductFragment : BaseFragment(), ProductRecyclerAdapter.OnItemClickListener {
    @JvmField
    @BindView(R.id.recyclerView)
    var recyclerView: RecyclerView? = null
    override fun getViewId(): Int {
        return R.layout.fragment_product
    }

    override fun onViewCreated(view: View) {
        val productRecyclerAdapter = ProductRecyclerAdapter(context, productsList)
        productRecyclerAdapter.setOnItemClickListener(this)
        recyclerView!!.layoutManager = GridLayoutManager(context, 2)
        recyclerView!!.adapter = productRecyclerAdapter
    }

    override fun onItemClick(productModel: ProductModel?) {
        val product = ProductEntityModel()
        product.id = productModel!!.id
        product.name = productModel.title
        product.description = productModel.description
        product.price = BigDecimal.valueOf(productModel.price!!.toLong())
        product.image = productModel.image
        val cart = CartHelper.getCart()
        cart.add(product, 1)
        val intent = Intent(context, CartActivity::class.java)
        startActivity(intent)
        activity!!.invalidateOptionsMenu()
    }
}