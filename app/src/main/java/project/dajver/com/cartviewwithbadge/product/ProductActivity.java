package project.dajver.com.cartviewwithbadge.product

import project.dajver.com.cartviewwithbadge.BaseActivity
import project.dajver.com.cartviewwithbadge.R

abstract class ProductActivity : BaseActivity() {
    override fun getViewId(): Int {
        return R.layout.activity_product
    }

    override fun onCreateView() {}
}