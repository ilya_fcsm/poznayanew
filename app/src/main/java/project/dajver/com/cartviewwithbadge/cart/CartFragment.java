package project.dajver.com.cartviewwithbadge.cart

import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import butterknife.BindView
import butterknife.OnClick
import org.json.JSONException
import org.json.JSONObject
import project.dajver.com.cartviewwithbadge.BaseFragment
import project.dajver.com.cartviewwithbadge.R
import project.dajver.com.cartviewwithbadge.cart.adapter.CartRecyclerAdapter
import project.dajver.com.cartviewwithbadge.cart.helper.CartHelper
import project.dajver.com.cartviewwithbadge.cart.helper.entity.models.CartItemsEntityModel
import ru.evotor.framework.core.IntegrationException
import ru.evotor.framework.core.IntegrationManagerFuture
import ru.evotor.framework.core.action.command.open_receipt_command.OpenSellReceiptCommand
import ru.evotor.framework.core.action.event.receipt.changes.position.PositionAdd
import ru.evotor.framework.core.action.event.receipt.changes.position.SetExtra
import ru.evotor.framework.receipt.ExtraKey
import ru.evotor.framework.receipt.Position
import java.math.BigDecimal
import java.util.*

abstract class CartFragment : BaseFragment(), CartRecyclerAdapter.OnItemClickListener {
    @JvmField
    @BindView(R.id.recyclerView)
    var recyclerView: RecyclerView? = null
    private var productRecyclerAdapter: CartRecyclerAdapter? = null
    private val Arr: ArrayList<Any>? = null
    override fun getViewId(): Int {
        return R.layout.fragment_cart
    }

    override fun onViewCreated(view: View) {
        onUpdateList()
    }

    override fun onItemClick(cartItemsEntityModel: CartItemsEntityModel) {
        // open details of product
    }

    override fun onItemPlusClicked(position: Int, cartItemsEntityModel: CartItemsEntityModel) {
        var quantity = cartItemsEntityModel.quantity
        val cartModel = CartItemsEntityModel()
        cartModel.product = cartItemsEntityModel.product
        quantity++
        cartModel.quantity = quantity
        productRecyclerAdapter!!.updateItem(position, cartModel)
    }

    override fun onItemMinusClicked(position: Int, cartItemsEntityModel: CartItemsEntityModel) {
        var quantity = cartItemsEntityModel.quantity
        val cartModel = CartItemsEntityModel()
        cartModel.product = cartItemsEntityModel.product
        quantity--
        cartModel.quantity = quantity
        productRecyclerAdapter!!.updateItem(position, cartModel)
    }

    override fun onUpdateList() {
        productRecyclerAdapter = CartRecyclerAdapter(context, CartHelper.getCartItems())
        productRecyclerAdapter!!.setOnItemClickListener(this)
        recyclerView!!.layoutManager = LinearLayoutManager(context)
        recyclerView!!.adapter = productRecyclerAdapter
    }

    @OnClick(R.id.buyButton)
    fun onBuyClick() {
        openReceipt()
    }

    fun openReceipt() {
        //Дополнительное поле для позиции. В списке наименований расположено под количеством и выделяется синим цветом
        val set: MutableSet<ExtraKey> = HashSet()
        set.add(ExtraKey(null, null, "Тест Зубочистки"))
        //Создание списка товаров чека
        val positionAddList: MutableList<PositionAdd> = ArrayList()
        positionAddList.add(
                PositionAdd(
                        Position.Builder.newInstance( //UUID позиции
                                UUID.randomUUID().toString(),  //UUID товара
                                null,  //Наименование
                                "Зубочистки",  //Наименование единицы измерения
                                "кг",  //Точность единицы измерения
                                0,  //Цена без скидок
                                BigDecimal(200),  //Количество
                                BigDecimal(1) //Добавление цены с учетом скидки на позицию. Итог = price - priceWithDiscountPosition
                        ).setPriceWithDiscountPosition(BigDecimal(100))
                                .setExtraKeys(set).build()
                )
        )

        //Дополнительные поля в чеке для использования в приложении
        val `object` = JSONObject()
        try {
            `object`.put("someSuperKey", "AWESOME RECEIPT OPEN")
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val extra = SetExtra(`object`)

        //Открытие чека продажи. Передаются: список наименований, дополнительные поля для приложения
        OpenSellReceiptCommand(positionAddList, extra).process(activity!!) { future: IntegrationManagerFuture ->
            try {
                val result = future.result
                if (result.type == IntegrationManagerFuture.Result.Type.OK) {
                    startActivity(Intent("evotor.intent.action.payment.SELL"))
                }
            } catch (e: IntegrationException) {
                e.printStackTrace()
            }
        }
    }
}