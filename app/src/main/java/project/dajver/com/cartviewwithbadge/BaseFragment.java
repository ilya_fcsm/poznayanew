package project.dajver.com.cartviewwithbadge

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife

abstract class BaseFragment : Fragment() {
    var context: Activity? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context = activity
        onViewCreated(view)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(getViewId, container, false)
        ButterKnife.bind(this, rootView)
        setHasOptionsMenu(true)
        return rootView
    }

    abstract val getViewId: Int
    abstract fun onViewCreated(view: View?)
}