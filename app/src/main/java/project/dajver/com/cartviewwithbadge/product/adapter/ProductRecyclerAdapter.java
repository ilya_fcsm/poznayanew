package project.dajver.com.cartviewwithbadge.product.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.squareup.picasso.Picasso
import project.dajver.com.cartviewwithbadge.R
import project.dajver.com.cartviewwithbadge.product.adapter.models.ProductModel

class ProductRecyclerAdapter(private val context: Context, private val catalogModels: List<ProductModel>) : RecyclerView.Adapter<ViewHolder>() {
    private var onItemClickListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        return ReceiveViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val viewHolder = holder as ReceiveViewHolder
        viewHolder.title!!.text = catalogModels[position].title
        viewHolder.description!!.text = catalogModels[position].description
        viewHolder.price!!.text = String.format(context.getString(R.string.dollars_format), catalogModels[position].price)
        Picasso.with(context).load(catalogModels[position].image).into(viewHolder.image)
    }

    override fun getItemCount(): Int {
        return catalogModels.size
    }

    inner class ReceiveViewHolder(itemView: View?) : ViewHolder(itemView) {
        @JvmField
        @BindView(R.id.title)
        var title: TextView? = null

        @JvmField
        @BindView(R.id.description)
        var description: TextView? = null

        @JvmField
        @BindView(R.id.image)
        var image: ImageView? = null

        @JvmField
        @BindView(R.id.price)
        var price: TextView? = null

        @JvmField
        @BindView(R.id.buyButton)
        var buyButton: Button? = null

        init {
            ButterKnife.bind(this, itemView!!)
            buyButton!!.setOnClickListener { view: View? -> onItemClickListener!!.onItemClick(catalogModels[adapterPosition]) }
        }
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
        this.onItemClickListener = onItemClickListener
    }

    interface OnItemClickListener {
        fun onItemClick(productModel: ProductModel?)
    }

}