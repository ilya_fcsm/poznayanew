package project.dajver.com.cartviewwithbadge.etc

import project.dajver.com.cartviewwithbadge.product.adapter.models.ProductModel
import java.util.*

object ProductsHelper {
    @JvmStatic
    val productsList: List<ProductModel>
        get() {
            val productModels: MutableList<ProductModel> = ArrayList()
            var model = ProductModel()
            model.id = 5678L
            model.title = "Позы"
            model.description = "Восточные"
            model.image = "pozy1.jpg"
            model.price = 47
            productModels.add(model)
            model = ProductModel()
            model.id = 5672L
            model.title = "Чебурек"
            model.description = "С мясом"
            model.image = "cheburek.jpg"
            model.price = 70
            productModels.add(model)
            model = ProductModel()
            model.id = 5673L
            model.title = "Картофель фри"
            model.description = " "
            model.image = "https://github.com/dajver/CartWithBadgeExample/blob/master/images/iphone.jpg?raw=true"
            model.price = 100
            productModels.add(model)
            model = ProductModel()
            model.id = 5674L
            model.title = "Салат Цезарь"
            model.description = "С курицей"
            model.image = "https://github.com/dajver/CartWithBadgeExample/blob/master/images/chuwi.jpg?raw=true"
            model.price = 200
            productModels.add(model)
            model = ProductModel()
            model.id = 5675L
            model.title = "Морс брусничный"
            model.description = "0.2л"
            model.image = "https://github.com/dajver/CartWithBadgeExample/blob/master/images/batary.jpg?raw=true"
            model.price = 40
            productModels.add(model)
            model = ProductModel()
            model.id = 5611L
            model.title = "Уха"
            model.description = " "
            model.image = "https://github.com/dajver/CartWithBadgeExample/blob/master/images/batary.jpg?raw=true"
            model.price = 120
            productModels.add(model)
            model = ProductModel()
            model.id = 5612L
            model.title = "Пюре"
            model.description = " "
            model.image = "https://github.com/dajver/CartWithBadgeExample/blob/master/images/batary.jpg?raw=true"
            model.price = 50
            productModels.add(model)
            model = ProductModel()
            model.id = 5613L
            model.title = "Котлета по-киевски"
            model.description = " "
            model.image = "https://github.com/dajver/CartWithBadgeExample/blob/master/images/batary.jpg?raw=true"
            model.price = 120
            productModels.add(model)
            model = ProductModel()
            model.id = 5614L
            model.title = "Американо"
            model.description = "0.33л"
            model.image = "https://github.com/dajver/CartWithBadgeExample/blob/master/images/batary.jpg?raw=true"
            model.price = 80
            productModels.add(model)
            model = ProductModel()
            model.id = 5615L
            model.title = "Рис с овощами"
            model.description = " "
            model.image = "https://github.com/dajver/CartWithBadgeExample/blob/master/images/batary.jpg?raw=true"
            model.price = 50
            productModels.add(model)
            return productModels
        }
}