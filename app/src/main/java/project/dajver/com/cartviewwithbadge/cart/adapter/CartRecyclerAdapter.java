package project.dajver.com.cartviewwithbadge.cart.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.squareup.picasso.Picasso
import project.dajver.com.cartviewwithbadge.R
import project.dajver.com.cartviewwithbadge.cart.helper.CartHelper
import project.dajver.com.cartviewwithbadge.cart.helper.entity.models.CartItemsEntityModel

class CartRecyclerAdapter(private val context: Context, private val productEntityModel: MutableList<CartItemsEntityModel>) : RecyclerView.Adapter<ViewHolder>() {
    private var onItemClickListener: OnItemClickListener? = null
    fun updateItem(position: Int, cartItemsEntityModel: CartItemsEntityModel) {
        if (cartItemsEntityModel.quantity > 0) {
            productEntityModel[position] = cartItemsEntityModel
            CartHelper.getCart().update(cartItemsEntityModel.product, cartItemsEntityModel.quantity)
        } else {
            CartHelper.getCart().remove(productEntityModel[position].product)
            onItemClickListener!!.onUpdateList()
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cart, parent, false)
        return ReceiveViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val viewHolder = holder as ReceiveViewHolder
        viewHolder.name!!.text = productEntityModel[position].product.name
        viewHolder.description!!.text = productEntityModel[position].product.description
        viewHolder.price!!.text = String.format(context.getString(R.string.dollars_format), productEntityModel[position].product.price)
        viewHolder.quantity!!.text = productEntityModel[position].quantity.toString()
        Picasso.with(context).load(productEntityModel[position].product.image).into(viewHolder.image)
    }

    override fun getItemCount(): Int {
        return productEntityModel.size
    }

    inner class ReceiveViewHolder(itemView: View) : ViewHolder(itemView) {
        @JvmField
        @BindView(R.id.name)
        var name: TextView? = null

        @JvmField
        @BindView(R.id.description)
        var description: TextView? = null

        @JvmField
        @BindView(R.id.image)
        var image: ImageView? = null

        @JvmField
        @BindView(R.id.price)
        var price: TextView? = null

        @JvmField
        @BindView(R.id.quantity)
        var quantity: TextView? = null

        @JvmField
        @BindView(R.id.plus)
        var plus: Button? = null

        @JvmField
        @BindView(R.id.minus)
        var minus: Button? = null

        init {
            ButterKnife.bind(this, itemView)
            itemView.setOnClickListener { view: View? -> onItemClickListener!!.onItemClick(productEntityModel[adapterPosition]) }
            minus!!.setOnClickListener { view: View? -> onItemClickListener!!.onItemMinusClicked(adapterPosition, productEntityModel[adapterPosition]) }
            plus!!.setOnClickListener { view: View? -> onItemClickListener!!.onItemPlusClicked(adapterPosition, productEntityModel[adapterPosition]) }
        }
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
        this.onItemClickListener = onItemClickListener
    }

    interface OnItemClickListener {
        fun onItemClick(cartItemsEntityModel: CartItemsEntityModel?)
        fun onItemPlusClicked(position: Int, cartItemsEntityModel: CartItemsEntityModel?)
        fun onItemMinusClicked(position: Int, cartItemsEntityModel: CartItemsEntityModel?)
        fun onUpdateList()
    }

}